import aiml

if __name__ == '__main__':

	# Create the kernel and learn AIML files
	kernel = aiml.Kernel()
	kernel.learn('default.aiml')
	kernel.learn('data.aiml')
	kernel.respond('INITIAL')

	# Press CTRL-C to break this loop
	while True:
		user_input = raw_input("Enter your message >> ")
		print kernel.respond(user_input.strip())
