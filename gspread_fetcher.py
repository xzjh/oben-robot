import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials
import xml.etree.ElementTree as etree
from xml.dom import minidom
import codecs

spreadsheet_key = '1fpNK_bXcUt-UTWtKiK-Pf54NV1DiqvfYhd_GADN_SwM'
api_config_file = 'gspread_api.json'

if __name__ == '__main__':
	
	json_key = json.load(open(api_config_file))
	scope = ['https://spreadsheets.google.com/feeds']

	credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope)

	gc = gspread.authorize(credentials)
	ss = gc.open_by_key(spreadsheet_key)
	wk_restaurants = ss.worksheet('Restaurants & Cafe')
	wk_shows = ss.worksheet('Shows')
	wk_activities = ss.worksheet('Recreational Activities')
	wk_nightlife = ss.worksheet('Night Life')

	data_restaurants = wk_restaurants.get_all_values()
	data_shows = wk_shows.get_all_values()
	data_activities = wk_activities.get_all_values()
	data_nightlife = wk_nightlife.get_all_values()
	restaurants_by_style = {}
	shows_by_type = {}
	activities_by_type = {}
	nightlife_by_type = {}
	output_content = '<?xml version="1.0" encoding="utf-8"?>\n\n'
	xml_aiml = etree.Element('aiml')

	#restaurants
	restaurant_general_list = []
	for this_restaurant in data_restaurants:
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = this_restaurant[1].upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = this_restaurant[1] + '\nWebsite: ' + this_restaurant[4] + '\nReview: ' + this_restaurant[5]

		#classified by styles
		if this_restaurant[2] in restaurants_by_style:
			restaurants_by_style[this_restaurant[2]].append(this_restaurant[1])
		else:
			restaurants_by_style[this_restaurant[2]] = [this_restaurant[1]]

		#add to general list
		restaurant_general_list.append(this_restaurant[1])

	xml_category = etree.SubElement(xml_aiml, 'category')
	xml_pattern = etree.SubElement(xml_category, 'pattern')
	xml_pattern.text = 'FOOD'
	xml_template = etree.SubElement(xml_category, 'template')
	xml_template.text = ', '.join([str(i + 1) + '. ' + restaurant_general_list[i] for i in range(len(restaurant_general_list))])

	for (restaurant_style, restaurant_list) in restaurants_by_style.iteritems():
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = (restaurant_style + ' FOOD').upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = ', '.join([str(i + 1) + '. ' + restaurant_list[i] for i in range(len(restaurant_list))])

	#shows
	shows_general_list = []
	for this_show in data_shows:
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = this_show[1].upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = this_show[1] + '\nPerformer: ' + this_show[2] + '\nWebsite: ' + this_show[4] + '\nReview: ' + this_show[5]

		#classified by styles
		if this_show[3] in shows_by_type:
			shows_by_type[this_show[3]].append(this_show[1])
		else:
			shows_by_type[this_show[3]] = [this_show[1]]

		#add to general list
		shows_general_list.append(this_show[1])

	xml_category = etree.SubElement(xml_aiml, 'category')
	xml_pattern = etree.SubElement(xml_category, 'pattern')
	xml_pattern.text = 'SHOWS'
	xml_template = etree.SubElement(xml_category, 'template')
	xml_template.text = ', '.join([str(i + 1) + '. ' + shows_general_list[i] for i in range(len(shows_general_list))])

	for (show_type, show_list) in shows_by_type.iteritems():
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = show_type.upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = ', '.join([str(i + 1) + '. ' + show_list[i] for i in range(len(show_list))])

	#activities
	activities_general_list = []
	for this_activity in data_activities:
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = this_activity[1].upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = this_activity[1] + '\nType: ' + this_activity[2] + '\nWebsite: ' + this_activity[3] + '\nAddress: ' + this_activity[4] + '\nPhone: ' + this_activity[5]

		#classified by styles
		if this_activity[2] in activities_by_type:
			activities_by_type[this_activity[2]].append(this_activity[1])
		else:
			activities_by_type[this_activity[2]] = [this_activity[1]]

		activities_general_list.append(this_activity[1])

	xml_category = etree.SubElement(xml_aiml, 'category')
	xml_pattern = etree.SubElement(xml_category, 'pattern')
	xml_pattern.text = 'ACTIVITY'
	xml_template = etree.SubElement(xml_category, 'template')
	xml_template.text = ', '.join([str(i + 1) + '. ' + activities_general_list[i] for i in range(len(activities_general_list))])

	for (activity_type, activity_list) in activities_by_type.iteritems():
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = activity_type.upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = ', '.join([str(i + 1) + '. ' + activity_list[i] for i in range(len(activity_list))])

	#nightlife
	nightlife_general_list = []
	for this_nightlife in data_nightlife:
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = this_nightlife[1].upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = this_nightlife[1] + '\nType: ' + this_nightlife[2] + '\nWebsite: ' + this_nightlife[3]

		#classified by styles
		if this_nightlife[2] in nightlife_by_type:
			nightlife_by_type[this_nightlife[2]].append(this_nightlife[1])
		else:
			nightlife_by_type[this_nightlife[2]] = [this_nightlife[1]]

		nightlife_general_list.append(this_nightlife[1])

	xml_category = etree.SubElement(xml_aiml, 'category')
	xml_pattern = etree.SubElement(xml_category, 'pattern')
	xml_pattern.text = 'NIGHT LIFE'
	xml_template = etree.SubElement(xml_category, 'template')
	xml_template.text = ', '.join([str(i + 1) + '. ' + nightlife_general_list[i] for i in range(len(nightlife_general_list))])

	for (nightlife_type, nightlife_list) in nightlife_by_type.iteritems():
		xml_category = etree.SubElement(xml_aiml, 'category')
		xml_pattern = etree.SubElement(xml_category, 'pattern')
		xml_pattern.text = nightlife_type.upper()
		xml_template = etree.SubElement(xml_category, 'template')
		xml_template.text = ', '.join([str(i + 1) + '. ' + nightlife_list[i] for i in range(len(nightlife_list))])

	output_content += etree.tostring(xml_aiml)
	output_content = minidom.parseString(output_content).toprettyxml(indent = '  ')

	with codecs.open('data.aiml', 'w', 'utf-8') as file_output:
		file_output.write(output_content)
